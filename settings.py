import configparser
import os


class ConfigFileError(Exception):
	pass

def get_learningwithtext_info():
	config = configparser.ConfigParser()
	dir_path = os.path.dirname(os.path.realpath(__file__))
	config.read(dir_path+"/config.ini")

	if 'LEARNINGWITHTEXT' in config:
		if 'statistics_url' in config['LEARNINGWITHTEXT']:
			return config['LEARNINGWITHTEXT']['statistics_url']
		else:
			raise ConfigFileError("Config.ini is missing the statistics_url property. Please add this information to config.ini.")
	else:
		raise  ConfigFileError("Config.ini is missing a Learning With Text section. Please add this information to config.ini.")



def get_beeminder_info():
	config = configparser.ConfigParser()
	dir_path = os.path.dirname(os.path.realpath(__file__))
	config.read(dir_path+"/config.ini")

	if 'BEEMINDER' in config:
		if 'USER' in config['BEEMINDER'] and 'API_KEY' in config['BEEMINDER'] and 'GOAL' in config['BEEMINDER']:
			beeminder = config['BEEMINDER']

			beeminder_info = dict(USER="", API_KEY="", GOAL="")

			beeminder_info['USER']= config['BEEMINDER']['USER']
			beeminder_info['API_KEY']= config['BEEMINDER']['API_KEY']
			beeminder_info['GOAL']= config['BEEMINDER']['GOAL']

			return beeminder_info

		else:
			raise ConfigFileError("Config.ini is missing a property in the Beeminder Section. Please add this information to config.ini.")
	else:
		raise ConfigFileError("Config.ini is missing a Beeminder section. Please add this information to config.ini.")

def set_beeminder_info(ENABLED, USER,API_KEY,GOAL):
	config = configparser.ConfigParser()
	config['BEEMINDER'] = { 'USE': ENABLED,
							'USER': USER,
							'API_KEY': API_KEY,
							'GOAL': GOAL}

	with open('config.ini', 'w') as configfile:
		config.write(configfile)

def main():

	try:
		beeminder_info = get_beeminder_info()
		for k,v in beeminder_info.items():
			print (k, v)
	except ConfigFileError as e:
		print (e)

	try:
		learningwithtext_info = get_learningwithtext_info()
		print (learningwithtext_info)
	except ConfigFileError as e:
		print (e)

if __name__  == "__main__":
	main()