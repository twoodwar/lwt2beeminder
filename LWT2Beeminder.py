import requests, bs4, beeminder, datetime
import settings as s

#This gets the LWT Statistics page URL from the config.ini file
try:
	s.get_learningwithtext_info()
except s.ConfigFileError as e:
	print (e)
	quit()

#This gets the LWT Statistics page and saves it locally
try:
	lwt_page = requests.get(s.get_learningwithtext_info())
	lwt_page.raise_for_status()
except:
	print(f'Unable to reach {s.get_learningwithtext_info()}, please double check the statistics_url setting in config.ini.')
	quit()

#Now we created a Beautiful Soup object out of the page and extract all tags that have a class of status1, as the numbers we want are contained in the final <span> tag with this class
lwt_page_soup = bs4.BeautifulSoup(lwt_page.text, "html.parser")
selection = lwt_page_soup.select('.status1')

#Since we know the number is in the final selection with the class name of "status1" we can go save the last element of the array
total_terms_created = selection[len(selection)-1].getText()

#Remove the character breaks from the string
total_terms_created = total_terms_created.replace(u'\xa0',u' ')

#Now remove the whitespace from the string
total_terms_created = total_terms_created.strip()
total_terms_created = int(total_terms_created)

#Now we retrive the Beeminder information from the config.ini file
try:
	beeminder_info = s.get_beeminder_info()
except s.ConfigFileError as e:
	print (e)
	quit()

#This calls the beeminder.py module created by NArthur and uses it to send the total_terms_created to a beeminder goal
beeminderUser = beeminder.Beeminder()
beeminderUser.set_username(beeminder_info['USER'])
beeminderUser.set_token(beeminder_info['API_KEY'])
beeminderUser.create_datapoint(beeminder_info['GOAL'],total_terms_created,datetime.datetime.utcnow().timestamp(),"Number of Total Words in Learning With Texts, excluding words marked to ignore. Reported by LWT2Beeminder.py")