# LWT2Beeminder
A small script which scrapes the all time number of words created by [Learning With Text](https://learning-with-texts.sourceforge.io/), and reports it to an odometer [Beeminder](https://www.beeminder.com) goal.

## Getting Started

### Prerequisites

You will need:
* Python 3.9
* [Learning With Text](https://learning-with-texts.sourceforge.io/) installed locally
* A [Beeminder](https://www.beeminder.com) Account and odometer goal

### Installing

Clone the repository from Gitlab

```
git clone https://gitlab.com/twoodwar/lwt2beeminder.git
```

#### Installing Dependencies
After cloning the repo, use requirements.txt to download the required packages.
```
pip install -r requirements.txt
```
Download [NArthur](https://github.com/narthur)'s [beeminder.py](https://github.com/narthur/pyminder/blob/master/pyminder/beeminder.py) into the same directory LWT2Beeminder.py lives.
```
curl https://github.com/narthur/pyminder/blob/master/pyminder/beeminder.py > beeminder.py
```
#### Usage
##### Config.ini
Open `config.ini` in a text editor and edit it with your information. 
```
[LEARNINGWITHTEXT]
statistics_url = http://localhost/lwt/statistics.php

[BEEMINDER]
user = <--Insert Beeminder Usename Here-->
api_key = <--Insert Beeminder API Key Here-->
goal = <--Insert Beeminder Goal Name Here-->
```
Run using Python to execute the script and add a data point to the Beeminder Goal
```
python LWT2Beeminder.py
```
## Built With

* Python 3.9

## Versioning

This repo uses [SemVer](http://semver.org/) for versioning. 

## Authors

* **Tyler Woodward** - [twoodwar](https://gitlab.com/twoodwar)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgements

* Thank you to [NArthur](https://github.com/narthur) for [Beeminder.py](https://github.com/narthur/pyminder), which LWT2Beeminder is dependent on for Beeminder Access.